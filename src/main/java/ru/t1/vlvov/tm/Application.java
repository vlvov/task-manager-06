package ru.t1.vlvov.tm;

import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        if (processArguments(args)) exit();
        processCommands();
    }

    private static boolean processArguments(final String args[]) {
        if (args == null || args.length == 0) return false;
        final String argument = args[0];
        processArgument(argument);
        return true;
    }

    private static void processArgument(final String argument) {
        switch (argument){
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            default:
                showErrorArgument();
        }
    }

    private static void processCommands() {
        System.out.println("** WELCOME TO TASK-MANAGER**");
        final Scanner scanner = new Scanner(System.in);
        String command;
        while(true) {
            System.out.println("ENTER COMMAND");
            command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(final String command) {
        if (command == null) {
            showErrorCommand();
            return;
        }
        switch (command){
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showErrorCommand();
        }
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Vladimir Lvov");
        System.out.println("E-mail: vlvov@t1.ru");
    }

    private static void showHelp() {
        System.out.printf("%s, %s - show application commands\n", TerminalConst.HELP, ArgumentConst.HELP);
        System.out.printf("%s, %s - show application version\n", TerminalConst.VERSION, ArgumentConst.VERSION);
        System.out.printf("%s, %s - show developer info\n", TerminalConst.ABOUT, ArgumentConst.ABOUT);
        System.out.printf("%s - exit \n", TerminalConst.EXIT);
    }

    private static void showErrorArgument() {
        System.err.println("Error! This argument not supported");
    }

    private static void showErrorCommand() {
        System.err.println("Error! This command not supported");
    }

}
